/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ResourceType string

// List of ResourceType
const (
	ACCOUNT_ROLES ResourceType = "account-roles"
	ACCOUNTS      ResourceType = "accounts"
	SIGNER_ROLES  ResourceType = "signer-roles"
	USERS         ResourceType = "users"
)
