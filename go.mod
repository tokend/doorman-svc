module gitlab.com/tokend/doorman-svc

go 1.15

require (
	github.com/alecthomas/kingpin v2.2.6+incompatible
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d // indirect
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef // indirect
	github.com/certifi/gocertifi v0.0.0-20200922220541-2c3bb06c6054 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/evalphobia/logrus_sentry v0.8.2 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/go-redis/redis/v8 v8.3.2
	github.com/google/jsonapi v0.0.0-20200825183604-3e3da1210d0c // indirect
	github.com/nullstyle/go-xdr v0.0.0-20180726165426-f4c839f75077 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/spf13/cast v1.3.0
	github.com/spf13/viper v1.7.1 // indirect
	gitlab.com/distributed_lab/ape v1.5.0
	gitlab.com/distributed_lab/figure v2.1.0+incompatible
	gitlab.com/distributed_lab/json-api-connector v0.2.3
	gitlab.com/distributed_lab/kit v1.8.1
	gitlab.com/distributed_lab/logan v3.7.2+incompatible
	gitlab.com/distributed_lab/lorem v0.2.0 // indirect
	gitlab.com/distributed_lab/running v0.0.0-20200706131153-4af0e83eb96c
	gitlab.com/tokend/connectors v0.1.3
	gitlab.com/tokend/go v3.13.0+incompatible
	gitlab.com/tokend/keypair v0.0.0-20190412110653-b9d7e0c8b312
	gitlab.com/tokend/regources v4.9.2-0.20210408150647-47ff1d870d56+incompatible
)
