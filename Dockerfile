FROM golang:1.12

WORKDIR /go/src/gitlab.com/tokend/doorman-svc
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/doorman-svc gitlab.com/tokend/doorman-svc

###

FROM alpine:3.9

COPY --from=0 /usr/local/bin/doorman-svc /usr/local/bin/doorman-svc
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["doorman-svc"]
