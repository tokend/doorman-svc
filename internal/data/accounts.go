package data

type AccountsQ interface {
	SaveAccountRole(accountID string, roleID uint64) error
	GetAccountRole(accountID string) (uint64, error)

	PutPublicKey(accountID, signerPublicKey string) error
	RemovePubKey(accountID, signerPublicKey string) error

	SetPubKeyRole(accountID string, roleID uint64) error
	GetPubKeyRole(accountID string) (uint64, error)
	RemovePubKeyRole(accountID string) error

	IsAccountSignerPublicKey(accountID, signerPublicKey string) (bool, error)

	// FIXME do we actually need this stuff?
	//  implement public keys deletion queue
	IsLastPage() bool
	SetLastPage(value bool) error

	GetCursor() (*uint64, error)
	SetCursor(cursor uint64) error
}
