package rds

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/doorman-svc/internal/data"
)

// FIXME do not silence errors !!!

const (
	LastPageKey = "lastPage"
	CursorKey   = "page[cursor]"
)

type accountsQ struct {
	ctx context.Context
	db  *redis.Client
}

func NewAuthenticationsQ(ctx context.Context, client *redis.Client) data.AccountsQ {
	return &accountsQ{
		ctx: ctx,
		db:  client,
	}
}

func (q *accountsQ) SaveAccountRole(accountID string, roleID uint64) error {
	return q.db.Set(q.ctx, fmt.Sprintf("%s#role", accountID), cast.ToString(roleID), 0).Err()
}

func (q *accountsQ) GetAccountRole(accountID string) (uint64, error) {
	rawRole, err := q.db.Get(q.ctx, fmt.Sprintf("%s#role", accountID)).Result()
	if err != nil {
		return 0, errors.Wrap(err, "failed to get account role")
	}

	return cast.ToUint64E(rawRole)
}

func (q *accountsQ) PutPublicKey(accountID, signerPublicKey string) error {
	return q.db.SAdd(q.ctx, accountID, signerPublicKey).Err()
}

func (q *accountsQ) RemovePubKey(accountID, signerPublicKey string) error {
	return q.db.SRem(q.ctx, accountID, signerPublicKey).Err()
}

func (q *accountsQ) SetPubKeyRole(accountID string, roleID uint64) error {
	return q.db.Set(q.ctx, fmt.Sprintf("%s#signer", accountID), cast.ToString(roleID), 0).Err()
}

func (q *accountsQ) GetPubKeyRole(accountID string) (uint64, error) {
	rawRole, err := q.db.Get(q.ctx, fmt.Sprintf("%s#signer", accountID)).Result()
	if err != nil {
		return 0, errors.Wrap(err, "failed to get account role")
	}

	return cast.ToUint64E(rawRole)
}

func (q *accountsQ) RemovePubKeyRole(accountID string) error {
	return q.db.Del(q.ctx, fmt.Sprintf("%s#signer", accountID)).Err()
}

func (q *accountsQ) IsAccountSignerPublicKey(accountID, signerPublicKey string) (bool, error) {
	return q.db.SIsMember(q.ctx, accountID, signerPublicKey).Result()
}

func (q *accountsQ) GetCursor() (*uint64, error) {
	key := fmt.Sprintf("%s", CursorKey)

	rawCursor, err := q.db.Get(q.ctx, key).Result()
	if err != nil {
		return nil, errors.Wrap(err, "failed to get cursor")
	}

	if rawCursor == "(nil)" {
		return nil, nil
	}

	cur, err := cast.ToUint64E(rawCursor)
	return &cur, err
}

func (q *accountsQ) SetCursor(cursor uint64) error {
	return q.db.Set(q.ctx, fmt.Sprintf("%s", CursorKey), cast.ToString(cursor), 0).Err()
}

func (q *accountsQ) IsLastPage() bool {
	err := q.db.Get(q.ctx, LastPageKey).Err()
	return err == nil
}

func (q *accountsQ) SetLastPage(isLastPage bool) error {
	if isLastPage {
		return q.db.Set(q.ctx, LastPageKey, "true", 0).Err()
	}
	return q.db.Del(q.ctx, LastPageKey).Err()
}
