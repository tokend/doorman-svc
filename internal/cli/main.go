package cli

import (
	"context"

	"github.com/alecthomas/kingpin"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/tokend/doorman-svc/internal/config"
	"gitlab.com/tokend/doorman-svc/internal/ingest"
	"gitlab.com/tokend/doorman-svc/internal/service"
)

func Run(args []string) bool {
	log := logan.New()
	ctx, cancel := context.WithCancel(context.Background())

	defer func() {
		if rvr := recover(); rvr != nil {
			log.WithRecover(rvr).Error("app panicked")
		}
		cancel()
	}()

	cfg := config.New(kv.MustFromEnv())
	log = cfg.Log()

	app := kingpin.New("doorman-svc", "")

	runCmd := app.Command("run", "run command")
	serviceCmd := runCmd.Command("service", "run service") // you can insert custom help
	streamerCmd := runCmd.Command("streamer", "run streamer")
	commonRun := runCmd.Command("all", "run streamer and one instance of API service")
	// custom commands go here...

	cmd, err := app.Parse(args[1:])
	if err != nil {
		log.WithError(err).Error("failed to parse arguments")
		return false
	}

	switch cmd {
	case serviceCmd.FullCommand():
		service.Run(ctx, cfg)
	// handle any custom commands here in the same way
	case streamerCmd.FullCommand():
		ingest.Run(ctx, cfg)
	case commonRun.FullCommand(): // TODO ensure graceful stop
		go func() {
			defer func() {
				if rvr := recover(); rvr != nil {
					log.WithRecover(rvr).Error("app panicked")
				}
			}()
			ingest.Run(ctx, cfg)
		}()
		service.Run(ctx, cfg)
	default:
		log.Errorf("unknown command %s", cmd)
		return false
	}

	return true
}
