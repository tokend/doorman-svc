package horizon

import (
	connector "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/tokend/doorman-svc/internal/config"
)

type Connector struct {
	*connector.Connector
	cfg config.Config
}

func NewConnector(cfg config.Config) *Connector {
	return &Connector{
		Connector: connector.NewConnector(cfg.Client()),
		cfg:       cfg,
	}
}
