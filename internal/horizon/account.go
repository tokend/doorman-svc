package horizon

import (
	"fmt"
	"net/url"

	"gitlab.com/distributed_lab/json-api-connector/cerrors"
	"gitlab.com/distributed_lab/logan/v3/errors"
	regources "gitlab.com/tokend/regources/generated"
)

func (c *Connector) GetAccountByID(accountID string) (*regources.Account, error) {
	u, err := url.Parse(fmt.Sprintf("/v3/accounts/%s", accountID))
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse account url")
	}

	var res regources.AccountResponse
	if err := c.Get(u, &res); err != nil {
		if cerrors.NotFound(err) {
			return nil, nil
		}
		return nil, errors.Wrap(err, "failed to get account")
	}

	return &res.Data, nil
}

func (c *Connector) GetSignersByAccountId(accountID string) ([]regources.Signer, error) {
	u, err := url.Parse(fmt.Sprintf("/v3/accounts/%s/signers", accountID))
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse account url")
	}

	var res regources.SignerListResponse
	if err := c.Get(u, &res); err != nil {
		if cerrors.NotFound(err) {
			return nil, nil
		}
		return nil, errors.Wrap(err, "failed to get account")
	}

	return res.Data, nil
}
