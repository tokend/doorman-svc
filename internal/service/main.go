package service

import (
	"context"
	"net"
	"net/http"

	"github.com/go-redis/redis/v8"
	"gitlab.com/distributed_lab/kit/copus/types"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/doorman-svc/internal/config"
)

type service struct {
	ctx         context.Context
	cfg         config.Config
	log         *logan.Entry
	copus       types.Copus
	listener    net.Listener
	clientRedis *redis.Client
}

func (s *service) run() error {
	s.log.Info("service start")

	r := s.router()

	if err := s.copus.RegisterChi(r); err != nil {
		return errors.Wrap(err, "cop failed")
	}

	return http.Serve(s.listener, r)
}

func newService(ctx context.Context, cfg config.Config) *service {
	return &service{
		ctx:         ctx,
		log:         cfg.Log(),
		listener:    cfg.Listener(),
		clientRedis: cfg.Redis(),
		copus:       cfg.Copus(),
		cfg:         cfg,
	}
}

func Run(ctx context.Context, cfg config.Config) {
	if err := newService(ctx, cfg).run(); err != nil {
		panic(err)
	}
}
