package handlers

import (
	"net/http"

	"gitlab.com/tokend/doorman-svc/internal/httpsignatures"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

// Checks signature on the request and returns keyID provided in the signature.
// Renders any error (if occurred) to the client as 401 Unauthorized or 400 Bad Request HTTP errors
func authenticateRequester(w http.ResponseWriter, r *http.Request) string {
	sig, err := httpsignatures.FromRequest(r)
	if err != nil {
		Log(r).WithError(err).Error("failed to get signature from request")
		ape.RenderErr(w, problems.NotAllowed(&badSignatureError{err}))
		return ""
	}

	if !sig.IsValid(r) {
		Log(r).Error("signature invalid")
		ape.RenderErr(w, problems.NotAllowed())
		return ""
	}

	return sig.KeyID
}

type badSignatureError struct {
	error
}

func (e *badSignatureError) BadRequest() bool {
	return true
}

func isAllowed(w http.ResponseWriter, r *http.Request) bool {
	okayer := Okayer(r)

	ok, err := okayer.IsOK(r)
	if err != nil {
		ape.RenderErr(w, problems.NotAllowed(err))
		return false
	}

	if !ok {
		ape.RenderErr(w, problems.NotAllowed())
	}

	return ok
}
