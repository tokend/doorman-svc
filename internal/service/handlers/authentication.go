package handlers

import (
	"errors"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/tokend/doorman-svc/resources"
)

func Authentication(w http.ResponseWriter, r *http.Request) {
	requesterKeyID := authenticateRequester(w, r)
	if requesterKeyID == "" {
		return
	}

	requesterAccountID := r.Header.Get("Account-Id")
	if requesterAccountID == "" {
		ape.RenderErr(w, problems.BadRequest(validation.Errors{
			"Account-Id": errors.New("header not found"),
		})...)
		return
	}

	ok, err := AuthenticationsQ(r).IsAccountSignerPublicKey(requesterAccountID, requesterKeyID)
	if err != nil {
		Log(r).WithError(err).Error("failed to check account signer public key")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	if !ok {
		Log(r).WithFields(logan.F{
			"signer":  requesterKeyID,
			"account": requesterAccountID,
		}).Error("signer does not correspond to account id provided in header")
		ape.RenderErr(w, problems.NotAllowed(&badAccountIDError{
			errors.New("signer does not correspond to account id provided in header"),
		}))
		return
	}

	accountRoleID, err := AuthenticationsQ(r).GetAccountRole(requesterAccountID)
	if err != nil {
		Log(r).WithError(err).Error("failed to get account role from storage")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	signerRoleID, err := AuthenticationsQ(r).GetPubKeyRole(requesterKeyID)
	if err != nil {
		Log(r).WithError(err).Error("failed to get account role from storage")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	ape.Render(w, resources.UserResponse{
		Data: userResource(requesterAccountID, cast.ToString(accountRoleID), cast.ToString(signerRoleID)),
	})
}

func userResource(accountID, roleID, signerRoleID string) resources.User {
	return resources.User{
		Key: resources.MakeUserKey(accountID),
		Relationships: resources.UserRelationships{
			Role:       *resources.MakeAccountRoleKey(roleID).AsRelation(),
			SignerRole: *resources.MakeSignerRoleKey(signerRoleID).AsRelation(),
		},
	}
}

type badAccountIDError struct {
	error
}

func (e *badAccountIDError) BadRequest() bool {
	return true
}
