package handlers

import (
	"context"
	"net/http"

	"gitlab.com/tokend/doorman-svc/internal/service/okayer"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/tokend/doorman-svc/internal/data"
)

type ctxKey int

const (
	logCtxKey ctxKey = iota
	authenticationsQCtxKey
	okayerCtxKey
)

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func CtxAuthenticationsQ(client data.AccountsQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, authenticationsQCtxKey, client)
	}
}

func AuthenticationsQ(r *http.Request) data.AccountsQ {
	return r.Context().Value(authenticationsQCtxKey).(data.AccountsQ)
}

func CtxOkayer(okayer okayer.Okayer) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, okayerCtxKey, okayer)
	}
}

func Okayer(r *http.Request) okayer.Okayer {
	return r.Context().Value(okayerCtxKey).(okayer.Okayer)
}
