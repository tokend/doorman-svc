package handlers

import "net/http"

func AddAuthorizationPolicy(w http.ResponseWriter, r *http.Request) {
	// TODO this method might be used to add custom auth policies to
	//  okayer such as role constrains or multisig weight rules
	//  right now idk how to implement it but we definitely need this feature for
	//  e.g. external integrators or custom auth checks on some endpoints

	//  TODO
	//   policy should contain:
	//	  - request url (identifier of the resource)
	//	  - policy type (explicitly defined type to use in this service)
	//    - artifact to check (signature, ownerAccountID, etc.)

	w.WriteHeader(http.StatusNotImplemented)
}
