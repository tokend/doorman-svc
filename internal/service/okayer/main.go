package okayer

import (
	"net/http"
)

type Okayer interface {
	IsOK(r *http.Request) (bool, error)
}
