package service

import (
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	rds "gitlab.com/tokend/doorman-svc/internal/data/redis"
	"gitlab.com/tokend/doorman-svc/internal/policy/signers"
	"gitlab.com/tokend/doorman-svc/internal/service/handlers"
)

func (s *service) router() chi.Router {
	r := chi.NewRouter()

	authQ := rds.NewAuthenticationsQ(s.ctx, s.clientRedis)

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxAuthenticationsQ(authQ),
			handlers.CtxOkayer(signers.NewSignersChecker(s.log, signers.NewPolicy(authQ))),
		),
	)

	//r.Handle("/*", http.HandlerFunc(handlers.Authentication)) // TODO try to use it with traefik

	r.Route("/integrations/doorman", func(r chi.Router) {
		r.Handle("/authenticate", http.HandlerFunc(handlers.Authentication))

		//r.Put("/", handlers.AddAuthorizationPolicy)                      // TODO NOT IMPLEMENTED
		//r.Handle("/authorize", http.HandlerFunc(handlers.Authorization)) // TODO NOT IMPLEMENTED
	})

	return r
}
