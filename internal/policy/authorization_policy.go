package policy

import "net/http"

type Authorization interface {
	IsAllowed(r *http.Request) (bool, error)
}

type BadSignatureError struct {
	Err error
}

func (e *BadSignatureError) BadRequest() bool {
	return true
}

func (e *BadSignatureError) Error() string {
	return e.Err.Error()
}

type AuthorizationError struct {
	Err error
}

func (e *AuthorizationError) NotAllowed() bool {
	return true
}

func (e *AuthorizationError) Error() string {
	return e.Err.Error()
}
