package signers

import (
	"net/http"

	"gitlab.com/tokend/doorman-svc/internal/policy"

	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/doorman-svc/internal/data"
	"gitlab.com/tokend/doorman-svc/internal/httpsignatures"
)

type Policy struct {
	authQ data.AccountsQ
}

func NewPolicy(q data.AccountsQ) *Policy {
	return &Policy{authQ: q}
}

func (p *Policy) IsAllowed(r *http.Request) (bool, error) {
	candidates, err := getAccountIDsHeader(r)
	if err != nil {
		return false, err
	}

	sig, err := httpsignatures.FromRequest(r)
	if err != nil {
		return false, &policy.BadSignatureError{Err: err}
	}

	for _, signerPublicKey := range candidates {
		related, err := p.authQ.IsAccountSignerPublicKey(signerPublicKey, sig.KeyID)
		if err != nil {
			return false, err
		}

		if related {
			return true, nil
		}
	}

	return false, nil
}

func getAccountIDsHeader(r *http.Request) ([]string, error) {
	const (
		accountIDHeaderName = "account-id"
	)

	accountIDHeader := r.Header.Get(accountIDHeaderName)
	//accountIDHeader, ok := r.Header[accountIDHeaderName]
	if accountIDHeader == "" {
		return nil, &policy.BadSignatureError{Err: errors.New("no Account-Id header present in request")}
	}

	// doing this (instead of return cast.ToStringSliceE) to render validation errors outside
	accountIDs, err := cast.ToStringSliceE(accountIDHeader)
	if err != nil {
		return nil, &policy.BadSignatureError{Err: errors.Wrap(err, "failed to cast Account-Id header to []string")}
	}

	return accountIDs, nil
}
