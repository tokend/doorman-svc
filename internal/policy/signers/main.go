package signers

import (
	"net/http"

	"gitlab.com/tokend/doorman-svc/internal/policy"

	"gitlab.com/distributed_lab/logan/v3"
)

// TODO

type Checker struct {
	log           *logan.Entry
	signersPolicy policy.Authorization
}

func NewSignersChecker(log *logan.Entry, authorization policy.Authorization) *Checker {
	return &Checker{
		log:           log,
		signersPolicy: authorization,
	}
}

func (c *Checker) IsOK(r *http.Request) (bool, error) {
	// TODO do we need to authenticate request here? :hmm:
	//sig, err := httpsignatures.FromRequest(r)
	//if err != nil {
	//	return false, errors.Wrap(err, "failed to create signature from signature header")
	//}

	//if !sig.IsValid(r) {
	//	return false, errors.Wrap(err, "request signature is not valid")
	//}

	return c.signersPolicy.IsAllowed(r)
}
