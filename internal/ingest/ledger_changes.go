package ingest

import (
	"context"
	"fmt"
	"sync"
	"time"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/distributed_lab/running"
	"gitlab.com/tokend/doorman-svc/internal/data"
	"gitlab.com/tokend/go/xdr"
	regources "gitlab.com/tokend/regources/generated"
)

type ledgerChangesSaver struct {
	ctx     context.Context
	id      int
	storage data.AccountsQ
	wg      *sync.WaitGroup
	log     *logan.Entry
}

type ledgerChangeMsg struct {
	rawLedgerEntryChange regources.LedgerEntryChange
}

func newChangesSaver(ctx context.Context, log *logan.Entry, storage data.AccountsQ, wg *sync.WaitGroup, id int) *ledgerChangesSaver {
	return &ledgerChangesSaver{
		ctx:     ctx,
		storage: storage,
		wg:      wg,
		log:     log.WithField(serviceName, "ledger-changes-saver"),
		id:      id,
	}
}

func (s *ledgerChangesSaver) process(txMsgs chan ledgerChangeMsg) {
	defer func() {
		if rvr := recover(); rvr != nil {
			s.log.WithField("worker-id", s.id).WithRecover(rvr).Error("ledger changes worker panicked")
			s.wg.Done()
		}
	}()

	for {
		select {
		case msg := <-txMsgs:
			running.WithThreshold(s.ctx, s.log, fmt.Sprintf("ledger-changes-saver-worker-%d", s.id), func(ctx context.Context) (bool, error) {
				var leChange xdr.LedgerEntryChange

				err := xdr.SafeUnmarshalBase64(msg.rawLedgerEntryChange.Attributes.Payload, &leChange)
				if err != nil {
					return false, errors.Wrap(err, "failed to unmarshal ledger change")
				}

				switch leChange.EntryType() {
				case xdr.LedgerEntryTypeAccount:
					return true, processAccount(leChange, s.storage)
				case xdr.LedgerEntryTypeSigner:
					return true, processSigner(leChange, s.storage)
				}

				return true, nil
			}, 1*time.Second, 5*time.Second, 20) // guess 20 attempts is enough to understand that something is bad with the message
		case <-s.ctx.Done():
			return
		default:
			time.Sleep(500 * time.Millisecond)
		}
	}

}

func processAccount(accountChange xdr.LedgerEntryChange, client data.AccountsQ) error {
	switch accountChange.Type {
	case xdr.LedgerEntryChangeTypeCreated:
		return putAccountRole(client,
			accountChange.Created.Data.Account.AccountId.Address(),
			uint64(accountChange.Created.Data.Account.RoleId))
	case xdr.LedgerEntryChangeTypeUpdated:
		return putAccountRole(client,
			accountChange.Updated.Data.Account.AccountId.Address(),
			uint64(accountChange.Updated.Data.Account.RoleId))
	default:
		return nil
	}
}

func processSigner(signerChange xdr.LedgerEntryChange, client data.AccountsQ) error {
	switch signerChange.Type {
	case xdr.LedgerEntryChangeTypeCreated:
		if err := appendPubKey(signerChange.Created, client); err != nil {
			return errors.Wrap(err, "failed tp append signer")
		}
		if err := putPubKeyRole(client, signerChange.Created.Data.Signer.PubKey.ToString(), uint64(signerChange.Created.Data.Signer.RoleId)); err != nil {
			return errors.Wrap(err, "failed to append signer role")
		}
	case xdr.LedgerEntryChangeTypeRemoved:
		if err := removePubKey(signerChange.Removed, client); err != nil {
			return errors.Wrap(err, "failed to remove pub key")
		}
		if err := removePubKeyRole(client, signerChange.Removed.Signer.PubKey.ToString()); err != nil {
			return errors.Wrap(err, "failed to remove role signer")
		}
	}
	return nil
}

func appendPubKey(le *xdr.LedgerEntry, client data.AccountsQ) error {
	accountID := le.Data.Signer.AccountId.Address()
	pubKey := le.Data.Signer.PubKey.ToString()

	return client.PutPublicKey(accountID, pubKey)
}

func removePubKey(removedLedgerKey *xdr.LedgerKey, client data.AccountsQ) error {
	accountID := removedLedgerKey.Signer.AccountId.Address()
	pubKey := removedLedgerKey.Signer.PubKey.ToString()

	return client.RemovePubKey(accountID, pubKey)
}

func putPubKeyRole(client data.AccountsQ, accountID string, roleToSet uint64) error {
	return client.SetPubKeyRole(accountID, roleToSet)
}

func removePubKeyRole(client data.AccountsQ, accountID string) error {
	return client.RemovePubKeyRole(accountID)
}

func putAccountRole(client data.AccountsQ, accountID string, roleToSet uint64) error {
	return client.SaveAccountRole(accountID, roleToSet)
}
