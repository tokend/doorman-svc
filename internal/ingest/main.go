package ingest

import (
	"context"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cast"
	connector "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/json-api-connector/cerrors"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/distributed_lab/running"
	"gitlab.com/tokend/doorman-svc/internal/config"
	"gitlab.com/tokend/doorman-svc/internal/data"
	rds "gitlab.com/tokend/doorman-svc/internal/data/redis"
	horizon2 "gitlab.com/tokend/doorman-svc/internal/horizon"
	"gitlab.com/tokend/go/xdr"
	regources "gitlab.com/tokend/regources/generated"
)

const (
	serviceName = "service-name"
)

type listenerService struct {
	ctx context.Context
	cfg config.Config
	log *logan.Entry
	q   data.AccountsQ
}

func (s *listenerService) run() error {
	s.log.Info("listener start")

	if err := s.ensureAdminAccount(); err != nil {
		return errors.Wrap(err, "failed to init admin account")
	}

	ingestStartTime := s.ensureAccountsSignersSnapshot()

	s.runIngest(ingestStartTime)

	return nil
}

func newService(ctx context.Context, cfg config.Config) *listenerService {
	return &listenerService{
		ctx: ctx,
		log: cfg.Log(),
		q:   rds.NewAuthenticationsQ(context.Background(), cfg.Redis()),
		cfg: cfg,
	}
}

func Run(ctx context.Context, cfg config.Config) {
	if err := newService(ctx, cfg).run(); err != nil {
		panic(err)
	}
}

func (s *listenerService) runIngest(ingestStartTime time.Time) {
	ingestCtx, cancel := context.WithCancel(s.ctx)

	defer func() {
		if rvr := recover(); rvr != nil {
			s.log.WithRecover(rvr).Error("tx ingester panicked")
		}
		cancel()
	}()

	txListener := connector.NewStreamer(s.cfg.Client(), "/v3/transactions", &TxListenerParams{
		TimeAfter: ingestStartTime.Unix(),
		LedgerEntryTypes: []xdr.LedgerEntryType{
			xdr.LedgerEntryTypeAccount,
			xdr.LedgerEntryTypeSigner,
		},
		LedgerEntryChangeTypes: []xdr.LedgerEntryChangeType{
			xdr.LedgerEntryChangeTypeCreated,
			xdr.LedgerEntryChangeTypeUpdated,
			xdr.LedgerEntryChangeTypeRemoved,
		},
		IncludeLedgerEntryChanges: true,
	})

	txIngester := newIngester(ingestCtx, s.log, s.q, s.cfg.Service().IngestScale)

	go txIngester.run()

	running.WithBackOff(s.ctx, s.log, "tx-listener", func(ctx context.Context) error {
		var page regources.TransactionListResponse
		if err := txListener.Next(&page); err != nil {
			if err == connector.ErrDataEmpty {
				return nil
			}
			return errors.Wrap(err, "failed to get next page")
		}

		for _, tx := range page.Data {
			for _, leChangeKey := range tx.Relationships.LedgerEntryChanges.Data {
				txIngester.txMsgs <- ledgerChangeMsg{
					rawLedgerEntryChange: *page.Included.MustLedgerEntryChange(leChangeKey),
				}
			}
		}

		next, err := url.ParseQuery(page.Links.Next)
		if err != nil {
			return errors.Wrap(err, "failed to parse `next` link, seems like next iteration of txListener.Next will fail")
		}

		if rawCursor := next.Get("page[cursor]"); rawCursor != "" {
			cursor, err := cast.ToUint64E(rawCursor)
			if err != nil {
				return errors.Wrap(err, "failed to parse cursor from `next` link, seems like next iteration of txListener.Next will fail")
			}

			if err := s.q.SetCursor(cursor); err != nil {
				// hope that cursor from next iteration will succeed
				return errors.Wrap(err, "failed to save cursor")
			}
		}

		return nil
	}, 1*time.Second, 1*time.Second, 1*time.Second)
}

type TxListenerParams struct {
	TimeAfter                 int64
	LedgerEntryTypes          []xdr.LedgerEntryType
	LedgerEntryChangeTypes    []xdr.LedgerEntryChangeType
	IncludeLedgerEntryChanges bool
	Cursor                    uint64
}

func castLedgerEntryTypes(rawTypes []xdr.LedgerEntryType) []string {
	res := make([]string, 0, len(rawTypes))
	for _, raw := range rawTypes {
		res = append(res, fmt.Sprintf("%d", raw))
	}
	return res
}

func castLedgerEntryChangeTypes(rawTypes []xdr.LedgerEntryChangeType) []string {
	res := make([]string, 0, len(rawTypes))
	for _, raw := range rawTypes {
		res = append(res, fmt.Sprintf("%d", raw))
	}
	return res
}

func (p *TxListenerParams) Encode() string {
	values := url.Values{}

	values.Add("filter[after]", strconv.FormatInt(p.TimeAfter, 10))
	values.Add("filter[ledger_entry_changes.entry_types]", strings.Join(castLedgerEntryTypes(p.LedgerEntryTypes), ","))
	values.Add("filter[ledger_entry_changes.change_types]", strings.Join(castLedgerEntryChangeTypes(p.LedgerEntryChangeTypes), ","))
	values.Add("include", "ledger_entry_changes")
	values.Add("page[cursor]", fmt.Sprintf("%d", p.Cursor))
	values.Add("page[limit]", fmt.Sprintf("%d", 100))

	return values.Encode()
}

func (s *listenerService) initStorage() error {
	if err := s.ensureAdminAccount(); err != nil {
		return errors.Wrap(err, "failed to ensure admin account")
	}

	if err := s.ensureCursor(); err != nil {
		return errors.Wrap(err, "failed to ensure cursor")
	}

	return nil
}

func (s *listenerService) ensureAdminAccount() error {
	var info regources.HorizonStateResponse

	infoEndpoint, err := url.Parse("/v3/info")
	if err != nil {
		return errors.Wrap(err, "failed to parse info url")
	}

	horizon := horizon2.NewConnector(s.cfg)

	err = horizon.Get(infoEndpoint, &info)
	if err != nil {
		return errors.Wrap(err, "failed get url api")
	}

	account, err := horizon.GetAccountByID(info.Data.Attributes.MasterAccountId)
	if err != nil {
		return errors.Wrap(err, "failed to get master account")
	}

	// TODO tx
	if err := s.q.SaveAccountRole(account.ID, cast.ToUint64(account.Relationships.Role.Data.ID)); err != nil {
		return errors.Wrap(err, "failed to save master's account role")
	}

	signers, err := horizon.GetSignersByAccountId(info.Data.Attributes.MasterAccountId)
	if err != nil {
		return errors.Wrap(err, "failed to get master account")
	}

	for _, signer := range signers {
		if err := s.q.PutPublicKey(info.Data.Attributes.MasterAccountId, signer.ID); err != nil {
			return errors.Wrap(err, "failed to put public key master`s")
		}

		if err := s.q.SetPubKeyRole(signer.ID, cast.ToUint64(signer.Relationships.Role.Data.ID)); err != nil {
			return errors.Wrap(err, "failed to set role signers admin`s")
		}
	}

	return nil
}

func (s *listenerService) ensureCursor() error {
	cur, err := s.q.GetCursor()
	if err != nil {
		return errors.Wrap(err, "failed to get cursor")
	}

	if cur == nil {
		return s.q.SetCursor(0)
	}

	return nil
}

func (s *listenerService) ensureAccountsSignersSnapshot() time.Time {
	start := time.Now()
	accountsPager := connector.NewStreamer(s.cfg.Client(), "/v3/accounts", &AccountsParams{})

	running.UntilSuccess(s.ctx, s.log.WithField("who", "accounts_snapshoter"), "account-snapshoter", func(context.Context) (bool, error) {
		var page regources.AccountListResponse
		if err := accountsPager.Next(&page); err != nil {
			if cerrors.NoContent(err) {
				return true, nil
			}
			return false, errors.Wrap(err, "failed to get next page")
		}

		if len(page.Data) == 0 { // check just in case pager don't support NoContent
			return true, nil
		}

		for _, account := range page.Data {
			if err := s.q.SaveAccountRole(account.ID, cast.ToUint64(account.Relationships.Role.Data.ID)); err != nil {
				s.log.
					WithError(err).
					WithFields(logan.F{
						"account_id":   account.ID,
						"account_role": account.Relationships.Role.Data.ID,
					}).
					Warn("failed to save account role")
			}

			for _, signer := range account.Relationships.Signers.Data {
				if err := s.q.PutPublicKey(account.ID, signer.ID); err != nil {
					s.log.
						WithError(err).
						WithFields(logan.F{
							"account_id":     account.ID,
							"signer_pub_key": signer.ID,
						}).
						Warn("failed to save signer")
				}
				infoSigner := page.Included.MustSigner(signer)
				if err := s.q.SetPubKeyRole(infoSigner.ID, cast.ToUint64(infoSigner.Relationships.Role.Data.ID)); err != nil {
					s.log.
						WithError(err).
						WithFields(logan.F{
							"account_id":          account.ID,
							"signer_pub_key":      signer.ID,
							"signer_pub_key_role": infoSigner.Relationships.Role.Data.ID,
						}).
						Warn("failed to save role signer`s")
				}
			}
		}

		return false, nil
	}, 100*time.Millisecond, 100*time.Millisecond)

	return start
}

type AccountsParams struct {
}

func (p *AccountsParams) Encode() string {
	values := url.Values{}
	values.Add("include", "signers")
	values.Add("page[limit]", fmt.Sprintf("%d", 100))
	return values.Encode()
}
