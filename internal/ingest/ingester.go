package ingest

import (
	"context"
	"sync"

	"gitlab.com/tokend/doorman-svc/internal/data"

	"gitlab.com/distributed_lab/logan/v3"
)

type ingester struct {
	ctx     context.Context
	log     *logan.Entry
	scale   int
	storage data.AccountsQ
	txMsgs  chan ledgerChangeMsg
}

func newIngester(ctx context.Context, log *logan.Entry, storage data.AccountsQ, scale int) *ingester {
	return &ingester{
		ctx:     ctx,
		log:     log.WithField(serviceName, "ledger-changes-ingester"),
		storage: storage,
		scale:   scale,
		txMsgs:  make(chan ledgerChangeMsg),
	}
}

func (s *ingester) run() {
	var wg sync.WaitGroup
	wg.Add(s.scale)
	ctx, cancel := context.WithCancel(s.ctx)
	defer cancel()

	for i := 0; i < s.scale; i++ {
		changesSaver := newChangesSaver(ctx, s.log, s.storage, &wg, i)
		go changesSaver.process(s.txMsgs)
	}

	wg.Wait()
}
