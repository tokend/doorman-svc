// httpsignatures is a golang implementation of the http-signatures spec
// found at https://tools.ietf.org/html/draft-cavage-http-signatures
package httpsignatures

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"regexp"
	"strings"

	"github.com/pkg/errors"
)

var (
	ErrorNoSignatureHeader = errors.New("No Signature header found in request")
	signatureRegex         = regexp.MustCompile(`(\w+)="([^"]*)"`)
)

const (
	headerSignature            = "Signature"
	headerAuthorization        = "Authorization"
	headerRequestTarget        = "Request-Target"
	headerRealRequestTarget    = "Real-Request-Target"
	headerRealRequestTargetLow = "real-request-target"
	authScheme                 = "Signature "
	RequestTarget              = "(request-target)"
)

// Signature is the hashed key + headers, either from a request or a signer
type Signature struct {
	KeyID     string
	Algorithm Algorithm
	Headers   HeaderList
	Signature []byte
}

// FromString creates a new Signature from its encoded form,
// eg `accountId="a",keyId="b",algorithm="c",headers="d",signature="e"`
func FromString(in string) (*Signature, error) {
	res := Signature{}
	var key string
	var value string

	for _, m := range signatureRegex.FindAllStringSubmatch(in, -1) {
		key = m[1]
		value = m[2]

		switch key {
		case "keyId":
			res.KeyID = value
		case "algorithm":
			alg, err := GetAlgorithm(value)
			if err != nil {
				return nil, errors.Wrap(err, "failed to get algorithm")
			}
			res.Algorithm = alg
		case "headers":
			res.Headers = headerListFromString(value)
		case "signature":
			signature, err := base64.StdEncoding.DecodeString(value)
			if err != nil {
				return nil, errors.Wrap(err, "failed to decode signature")
			}
			res.Signature = signature
		}
	}

	if len(res.Signature) == 0 {
		return nil, errors.New("Missing signature")
	}

	if len(res.KeyID) == 0 {
		return nil, errors.New("Missing keyId")
	}

	if res.Algorithm == nil {
		return nil, errors.New("Missing algorithm")
	}

	return &res, nil
}

// FromRequest creates a new Signature from the Request
// both Signature and Authorization http headers are supported.
func FromRequest(r *http.Request) (*Signature, error) {
	if s, ok := r.Header[headerSignature]; ok {
		return FromString(s[0])
	}
	if a, ok := r.Header[headerAuthorization]; ok {
		return FromString(strings.TrimPrefix(a[0], authScheme))
	}
	return nil, ErrorNoSignatureHeader
}

// IsValid validates this signature for the given key
func (s Signature) IsValid(r *http.Request) bool {
	signingString, err := s.Headers.signingString(r)
	if err != nil {
		return false
	}

	return s.Algorithm.Verify(s, []byte(signingString))
}

type HeaderList []string

func (h HeaderList) String() string {
	return strings.ToLower(strings.Join(h, " "))
}

func (h HeaderList) hasDate() bool {
	for _, header := range h {
		if header == "date" {
			return true
		}
	}

	return false
}

func (h HeaderList) signingString(req *http.Request) (string, error) {
	lines := []string{}

	for _, header := range h {
		switch header {
		case RequestTarget:
			lines = append(lines, canonicalRequestTargetLine(req))
		case headerRealRequestTargetLow:
			lines = append(lines, realRequestTargetLine(req))
		default:
			line, err := headerLine(req, header)
			if err != nil {
				return "", err
			}
			lines = append(lines, line)
		}
	}

	return strings.Join(lines, "\n"), nil
}

func realRequestTargetLine(r *http.Request) string {
	realReqTargetURL := r.Header.Get(headerRealRequestTarget)
	if realReqTargetURL == "" && r.URL != nil {
		realReqTargetURL = r.URL.RequestURI()
	}
	return fmt.Sprintf("%s: %s", headerRealRequestTargetLow, realReqTargetURL) // Real-Request-Target is passed with method in signed.Client
}

func canonicalRequestTargetLine(r *http.Request) string {
	requestTargetURL := r.Header.Get(headerRequestTarget)

	if requestTargetURL == "" && r.URL != nil {
		requestTargetURL = r.URL.RequestURI()
	}

	return fmt.Sprintf("%s: %s", RequestTarget, RequestTargetValue(strings.ToLower(r.Method), requestTargetURL))
}

func RequestTargetValue(method, url string) string {
	return fmt.Sprintf("%s %s", strings.ToLower(method), url)
}

func headerLine(req *http.Request, header string) (string, error) {
	if value := req.Header.Get(header); value != "" {
		return fmt.Sprintf("%s: %s", header, value), nil
	}

	return "", errors.New(fmt.Sprintf("Missing required header '%s'", header))
}

func headerListFromString(list string) HeaderList {
	return strings.Split(strings.ToLower(string(list)), " ")
}
