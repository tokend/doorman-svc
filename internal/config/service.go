package config

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
)

type ServiceCfg struct {
	IngestScale int `fig:"ingest_scale"`
}

func (c *config) Service() *ServiceCfg {
	return c.service.Do(func() interface{} {
		cfg := ServiceCfg{
			IngestScale: 10,
		}

		if err := figure.Out(&cfg).From(kv.MustGetStringMap(c.getter, "service")).Please(); err != nil {
			panic(err)
		}

		return &cfg
	}).(*ServiceCfg)
}
