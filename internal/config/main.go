package config

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/copus"
	"gitlab.com/distributed_lab/kit/copus/types"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/tokend/connectors/signed"
)

type Config interface {
	comfig.Logger
	comfig.Listenerer
	types.Copuser
	signed.Clienter

	Redis() *redis.Client
	Service() *ServiceCfg
}

type config struct {
	comfig.Logger
	comfig.Listenerer
	types.Copuser
	signed.Clienter

	redis   comfig.Once
	service comfig.Once

	getter kv.Getter
}

func New(getter kv.Getter) Config {
	return &config{
		getter:     getter,
		Listenerer: comfig.NewListenerer(getter),
		Logger:     comfig.NewLogger(getter, comfig.LoggerOpts{}),
		Copuser:    copus.NewCopuser(getter),
		Clienter:   signed.NewClienter(getter),
	}
}
