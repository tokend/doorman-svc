package doorman

import (
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/tokend/doorman-svc/resources"
)

type Constraint interface {
	Check(user *resources.User) bool
	Description() string
}

type ConjunctiveChecker struct {
	log *logan.Entry
}

func NewConjunctiveChecker(log *logan.Entry) *ConjunctiveChecker {
	return &ConjunctiveChecker{log: log}
}

func (c *ConjunctiveChecker) CheckUserConstraints(user *resources.User, cons ...Constraint) bool {
	for _, con := range cons {
		if !con.Check(user) {
			c.log.WithField("desc", con.Description()).Error("constraint failed")
			return false
		}
	}
	return true
}

type DisjunctiveChecker struct {
	log *logan.Entry
}

func NewDisjunctiveChecker(log *logan.Entry) *DisjunctiveChecker {
	return &DisjunctiveChecker{log: log}
}

func (c *DisjunctiveChecker) CheckUserConstraints(user *resources.User, cons ...Constraint) bool {
	for _, con := range cons {
		if con.Check(user) {
			return true
		}
		c.log.WithField("desc", con.Description()).Error("constraint failed, trying next one")
	}
	return false
}
